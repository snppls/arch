[[ -f  ~/.bashrc ]] && ~/.bashrc

export PATH=$PATH:$HOME/.scripts
export EDITOR="vim"
xport TERMINAL="st"
export BROWSER="firefox"

if [[ "$(tty)" = "/dev/tty1" ]]; then
        pgrep dwm || startx
fi
